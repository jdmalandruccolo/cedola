//
//  CedolaThumbnailViewController.m
//  Cedola
//
//  Created by Joseph Malandruccolo on 4/28/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import "CedolaThumbnailViewController.h"
#import "CedolaDealThumbnailCell.h"
#import "CedolaAPIManager.h"
#import "GTMHTTPFetcher.h"
#import "CedolaYipitDeal.h"
#import "CedolaShowDealViewController.h"
#import "CedolaContainerViewController.h"

NSString * const    kShowSegueIdentifier = @"show deal";

@interface CedolaThumbnailViewController ()

@property (strong, nonatomic) NSCache *imageCache;

@end

@implementation CedolaThumbnailViewController



#pragma mark - life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.trackedViewName = @"thumbnail vc";
    NSLog(@"Cedolathumbnailvc view did load");
    //  line below not required as IB is used
    //[self.thumbnailCollectionView registerClass:[CedolaDealThumbnailCell class] forCellWithReuseIdentifier:@"thumbnail"];
    _imageCache = [[NSCache alloc] init];

}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kShowSegueIdentifier]) {
        NSLog(@"start seguing to deal");
        NSArray *arrayOfPaths = [self.thumbnailCollectionView indexPathsForSelectedItems];
        NSIndexPath *ip = [arrayOfPaths lastObject];
        
        CedolaYipitDeal *deal = [self.deals objectAtIndex:ip.item];
        CedolaShowDealViewController *showVC = (CedolaShowDealViewController*)[segue destinationViewController];
        [showVC setDeal:deal];
    }
}






#pragma mark - UICollectionViewDataSource Protocol
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.deals) {
        return [self.deals count];
    }
    else {
        return 0;
    }
}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifer = @"thumbnail";
    CedolaDealThumbnailCell *cell = (CedolaDealThumbnailCell*) [self.thumbnailCollectionView dequeueReusableCellWithReuseIdentifier:cellIdentifer forIndexPath:indexPath];
    CedolaYipitDeal *deal = [self.deals objectAtIndex:indexPath.item];
    
    
    
    cell.previewText.text = [deal title];
    cell.thumbnail.image = ![self.imageCache objectForKey:[deal dealId]] ? [UIImage imageNamed:@"cloud-download"] : [self.imageCache objectForKey:[deal dealId]];
    
    
    //  if there is no image in the cache, get the image the expensive way
    if (![self.imageCache objectForKey:[deal dealId]]) {
        NSURL *photoUrl = [NSURL URLWithString:[deal smallImageUrl]];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        GTMHTTPFetcher *imageFetcher = [GTMHTTPFetcher fetcherWithRequest:[NSURLRequest requestWithURL:photoUrl]];
        [imageFetcher beginFetchWithCompletionHandler:^(NSData *photoData, NSError *error) {
            if (error != nil) {
                NSLog(@"error downloading image");
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            }
            else {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                NSLog(@"fetch image successful");
                UIImage *photo = [UIImage imageWithData:photoData];
                if (photo != nil) {
                    [self.imageCache setObject:photo forKey:[deal dealId]];
                }
                cell.thumbnail.contentMode = UIViewContentModeScaleAspectFill;
                cell.thumbnail.image = photo;
                [self.thumbnailCollectionView reloadData];
            }
            
        }];
    }
    
    return cell;
    
}




@end
