//
//  CedolaShowDealViewController.m
//  Cedola
//
//  Created by Joseph Malandruccolo on 4/30/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import "CedolaShowDealViewController.h"
#import "GTMHTTPFetcher.h"
#import "YipitDeal+CedolaYipitDeal.h"
#import "YipitDeal.h"

@interface CedolaShowDealViewController ()

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@implementation CedolaShowDealViewController


#pragma mark - life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.trackedViewName = @"show vc";
    
    self.previewLabel.text = self.deal.title;
    self.description.text = self.deal.description;
    
    NSMutableString *priceText = [[NSMutableString alloc] initWithString:@"Price: "];
    self.price.text = [priceText stringByAppendingString:self.deal.price];
    
    NSMutableString *valueText = [[NSMutableString alloc] initWithString:@"Value: "];
    self.price.text = [valueText stringByAppendingString:self.deal.price];
    
    NSMutableString *sourceText = [NSMutableString stringWithString:@"Brought to you by "];
    self.source.text = [sourceText stringByAppendingString:self.deal.source];
    
    self.description.text = self.deal.description;
    
    NSURL *photoUrl = [NSURL URLWithString:self.deal.bigImageUrl];
    GTMHTTPFetcher *imageFetcher = [GTMHTTPFetcher fetcherWithRequest:[NSURLRequest requestWithURL:photoUrl]];
    [imageFetcher beginFetchWithCompletionHandler:^(NSData *photoData, NSError *error) {
        if (error != nil) {
            NSLog(@"error downloading image");
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
        else {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSLog(@"fetch image successful");
            UIImage *photo = [UIImage imageWithData:photoData];
            self.dealImageView.contentMode = UIViewContentModeScaleAspectFit;
            self.dealImageView.image = photo;
            [self.dealImageView setNeedsDisplay];
        }
        
    }];

}


- (void)viewWillAppear:(BOOL)animated
{
    if (!self.managedObjectContext) self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
}



#pragma mark - target/action
- (IBAction)buyButtonPressed:(UIButton *)sender
{
    NSLog(@"buy button pressed");
    //  take user to url to buy deal
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.deal.mobileUrl]];
    
}

- (IBAction)saveButtonPressed:(UIButton *)sender
{
    NSLog(@"save button pressed");
    //  add the deal to core data
    
    [YipitDeal yipitDealWithCedolaYipitDeal:self.deal inManagedObjectContext:self.managedObjectContext];
    
    
}


#pragma mark - core data
- (void)useDemoDocument
{
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentationDirectory inDomains:NSUserDomainMask] lastObject];
    
    url = [url URLByAppendingPathComponent:@"Demo Document"];
    UIManagedDocument *document = [[UIManagedDocument alloc] initWithFileURL:url];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[url path]]) {
        [document saveToURL:url
           forSaveOperation:UIDocumentSaveForCreating
          completionHandler:^(BOOL success){
            if (success) {
                self.managedObjectContext = document.managedObjectContext;
            }
        }];
    }
    else if (document.documentState == UIDocumentStateClosed) {
        [document openWithCompletionHandler:^(BOOL success) {
            self.managedObjectContext = document.managedObjectContext;
        }];
    }
    else {
        self.managedObjectContext = document.managedObjectContext;
    }
}






@end
