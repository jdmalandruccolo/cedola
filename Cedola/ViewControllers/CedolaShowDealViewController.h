//
//  CedolaShowDealViewController.h
//  Cedola
//
//  Created by Joseph Malandruccolo on 4/30/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CedolaYipitDeal.h"

@interface CedolaShowDealViewController : GAITrackedViewController
@property (weak, nonatomic) IBOutlet UIImageView *dealImageView;
@property (weak, nonatomic) IBOutlet UILabel *description;
@property (weak, nonatomic) IBOutlet UILabel *previewLabel;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *value;
@property (weak, nonatomic) CedolaYipitDeal *deal;
@property (weak, nonatomic) IBOutlet UILabel *source;

- (IBAction)buyButtonPressed:(UIButton *)sender;
- (IBAction)saveButtonPressed:(UIButton *)sender;
@end
