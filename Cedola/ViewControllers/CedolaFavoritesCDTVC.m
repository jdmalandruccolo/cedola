//
//  CedolaFavoritesCDTVC.m
//  Cedola
//
//  Created by Joseph Malandruccolo on 5/1/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import "CedolaFavoritesCDTVC.h"
#import "YipitDeal.h"



@implementation CedolaFavoritesCDTVC


- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    _managedObjectContext = managedObjectContext;
    
    if (managedObjectContext) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"YipitDeal"];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"value" ascending:NO]];
        request.predicate = nil;    //  all favorites
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    }
    else {
        self.fetchedResultsController = nil;
    }
}



#pragma mark - tableview method not provided by CDTVC
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"favorite cell"];
    
    YipitDeal *yipitDeal = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = yipitDeal.title;
    NSMutableString *valueText = [NSMutableString stringWithString:@"Value: "];
    cell.detailTextLabel.text = [valueText stringByAppendingString:yipitDeal.value];
    
    return cell;
    
}

@end
