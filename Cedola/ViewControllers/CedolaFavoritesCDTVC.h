//
//  CedolaFavoritesCDTVC.h
//  Cedola
//
//  Created by Joseph Malandruccolo on 5/1/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface CedolaFavoritesCDTVC : CoreDataTableViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
