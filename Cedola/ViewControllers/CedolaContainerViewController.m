//
//  CedolaContainerViewController.m
//  Cedola
//
//  Created by Joseph Malandruccolo on 4/29/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import "CedolaContainerViewController.h"
#import "CedolaThumbnailViewController.h"
#import "GTMHTTPFetcher.h"
#import "CedolaYipitDeal.h"
#import "CedolaAPIManager.h"

//  segue management
NSString * const    kContainer1Segue = @"container 1 segue";
NSString * const    kContainer1Category = @"activities-adventures";

NSString * const    kContainer2Segue = @"container 2 segue";
NSString * const    kContainer2Category = @"health-beauty";

NSString * const    kContainer3Segue = @"container 3 segue";
NSString * const    kContainer3Category = @"dining-nightlife";

NSString * const    kContainer4Segue = @"container 4 segue";
NSString * const    kContainer4Category = @"fitness";

NSString * const    kContainer5Segue = @"container 5 segue";
NSString * const    kContainer5Category = @"retail-services";

NSString * const    kSearchSegue = @"goto search";
NSString * const    kRefreshSegue = @"refresh results";


//  api management
NSString * const    kWaitingForNetworkText = @"fetching deals!";
NSString * const    kYipitApiKeyForResponse = @"response";
NSString * const    kYipitApiKeyForDeals = @"deals";

@interface CedolaContainerViewController ()
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)yipitLogoTapped:(UIButton *)sender;

@end

@implementation CedolaContainerViewController





#pragma mark - life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.trackedViewName = @"container vc";
    //  download the api data
    NSURLRequest *apiRequest;
    if (self.apiUrlRequest) {
        apiRequest = self.apiUrlRequest;
    }
    else {
        apiRequest = [[CedolaAPIManager sharedAPIManager] yipitAPIFFromCurrentLocation];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    GTMHTTPFetcher *apiFetcher = [GTMHTTPFetcher fetcherWithRequest:apiRequest];
    [apiFetcher beginFetchWithCompletionHandler:^(NSData *dealData, NSError *error) {
        if (error != nil) {
            NSLog(@"apiFetcher error: %@", error);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
        else {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSLog(@"downloaded deal data");
            NSDictionary *apiResults = [NSJSONSerialization JSONObjectWithData:dealData options:kNilOptions error:&error];
            NSLog(@"Raw api results: %@", apiResults);
            
            //  extract deals from JSON and store in self.deals
            NSArray *rawDeals = [NSArray arrayWithArray:[[apiResults objectForKey:kYipitApiKeyForResponse] objectForKey:kYipitApiKeyForDeals]];
            NSLog(@"Array of deals: %@", rawDeals);
            
            //  create Deal Objects with api data
            NSMutableArray *deals = [[NSMutableArray alloc] init];
            for (NSDictionary *dealDict in rawDeals) {
                
                CedolaYipitDeal *dealToAdd = [[CedolaYipitDeal alloc] initWithDealDictionary:dealDict];
                [deals addObject:dealToAdd];
                
            }
            
            for (CedolaThumbnailViewController *child in [self childViewControllers]) {
                NSLog(@"did enter for loop");
                NSLog(@"child category %@", [child category]);
                NSLog(@"deals for child: %@", [self dealsForCategory:[child category] inDeals:deals]);
                [child setDeals:[self dealsForCategory:[child category] inDeals:deals]];
                [child.thumbnailCollectionView reloadData];
            }
            
            [self sizeScrollView];
            
        }
    }];

}


- (void)viewDidAppear:(BOOL)animated
{
    //  use this method to control the size of the scroll view - not required if defining a runtime attribute in IB
    //[self sizeScrollView];
}


- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"CedoleThumbnailViewController viewWillAppear:");
    //[self sizeScrollView];
    
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"CedolaContainerViewController prepareForSegue called");
    
    if ([[segue identifier] isEqualToString:kContainer1Segue]) {
        CedolaThumbnailViewController *containerVC = (CedolaThumbnailViewController*) [segue destinationViewController];
        [containerVC setCategory:kContainer1Category];
    }
    
    if ([[segue identifier] isEqualToString:kContainer2Segue]) {
        CedolaThumbnailViewController *containerVC = (CedolaThumbnailViewController*) [segue destinationViewController];
        [containerVC setCategory:kContainer2Category];
    }
    
    if ([[segue identifier] isEqualToString:kContainer3Segue]) {
        CedolaThumbnailViewController *containerVC = (CedolaThumbnailViewController*) [segue destinationViewController];
        [containerVC setCategory:kContainer3Category];
    }
    
    if ([[segue identifier] isEqualToString:kContainer4Segue]) {
        CedolaThumbnailViewController *containerVC = (CedolaThumbnailViewController*) [segue destinationViewController];
        [containerVC setCategory:kContainer4Category];
    }
    
    if ([[segue identifier] isEqualToString:kContainer5Segue]) {
        CedolaThumbnailViewController *containerVC = (CedolaThumbnailViewController*) [segue destinationViewController];
        [containerVC setCategory:kContainer5Category];
    }
    
    
    if ([[segue identifier] isEqualToString:kSearchSegue]) {
        //  do nothing
    }
    
    if ([[segue identifier] isEqualToString:kRefreshSegue]) {
        CedolaContainerViewController *ccvc = (CedolaContainerViewController*) [segue destinationViewController];
        [ccvc setApiUrlRequest:nil];
    }
    
    
}

- (IBAction)unwindToThumbnails:(UIStoryboardSegue *)sender
{
    //  do nothing
}


- (void)sizeScrollView
{
    
    if ([[UIDevice currentDevice].model isEqualToString:@"iPhone"]) {
        [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, 1000)];
    }
    else {
        [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, 2000)];
    }

    
}

- (NSArray*) dealsForCategory: (NSString *)category inDeals:(NSArray *)deals
{
    NSMutableArray *properDeals = [[NSMutableArray alloc] init];
    for (CedolaYipitDeal *deal in deals) {
        NSLog(@"category for child: %@", category);
        NSLog(@"tag from deal: %@", [deal category]);
        if ([[deal category] isEqualToString:category]) {
            [properDeals addObject:deal];
        }
    }
    
    return properDeals;
}


#pragma mark - target/action
- (IBAction)yipitLogoTapped:(UIButton *)sender
{
    NSLog(@"yipit logo tapped");
}
@end
