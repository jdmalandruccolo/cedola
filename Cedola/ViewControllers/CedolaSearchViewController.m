//
//  CedolaSearchViewController.m
//  Cedola
//
//  Created by Joseph Malandruccolo on 4/30/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import "CedolaSearchViewController.h"
#import "CedolaSearchTermCell.h"
#import "CedolaAPIManager.h"
#import "CedolaContainerViewController.h"

NSString * const    kShowSearchResultsSegue = @"show search results";

@interface CedolaSearchViewController ()

@property (strong, nonatomic) NSArray *searchTerms;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
- (IBAction)fetchDealsButton:(UIButton *)sender;

@end

@implementation CedolaSearchViewController




#pragma mark - life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.trackedViewName = @"search vc";
    
    self.searchTerms = [[CedolaAPIManager sharedAPIManager] validSearchTerms];
    [self.pickerView reloadAllComponents];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.pickerView reloadAllComponents];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kShowSearchResultsSegue]) {
        CedolaContainerViewController *ccvc = (CedolaContainerViewController *)[segue destinationViewController];
        [ccvc setApiUrlRequest:[[CedolaAPIManager sharedAPIManager] yipitAPIFromTag:[self.searchTerms objectAtIndex:[self.pickerView selectedRowInComponent:0]]]];
    }
}


#pragma mark - picker view data source methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.searchTerms count];
}

#pragma mark - picket view delegate methods
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.searchTerms objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //  do nothing
    NSLog(@"did select row %ld in component: %ld", (long)row, (long)component);
}


- (IBAction)fetchDealsButton:(UIButton *)sender
{
    
}
@end
