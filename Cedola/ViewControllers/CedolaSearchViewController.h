//
//  CedolaSearchViewController.h
//  Cedola
//
//  Created by Joseph Malandruccolo on 4/30/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CedolaSearchViewController : GAITrackedViewController <UIPickerViewDataSource, UIPickerViewDelegate>


@end
