//
//  CedolaThumbnailViewController.h
//  Cedola
//
//  Created by Joseph Malandruccolo on 4/28/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CedolaThumbnailViewController : GAITrackedViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) NSString *category;
@property (strong, nonatomic) NSArray *deals;
@property (weak, nonatomic) IBOutlet UICollectionView *thumbnailCollectionView;


@end
