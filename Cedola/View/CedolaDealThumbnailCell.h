//
//  CedolaDealThumbnailCell.h
//  Cedola
//
//  Created by Joseph Malandruccolo on 4/29/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CedolaDealThumbnailCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (weak, nonatomic) IBOutlet UILabel *previewText;

@end
