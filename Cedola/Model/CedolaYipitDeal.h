//
//  CedolaYipitDeal.h
//  Cedola
//
//  Created by Joseph Malandruccolo on 4/29/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CedolaYipitDeal : NSObject

@property (strong, nonatomic) NSString *dealId;
@property (strong, nonatomic) NSString *endDate;
@property (strong, nonatomic) NSString *price;
@property (strong, nonatomic) NSString *value;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *yipitTitle;
@property (strong, nonatomic) NSString *smallImageUrl;
@property (strong, nonatomic) NSString *bigImageUrl;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSDictionary *address;
@property (strong, nonatomic) NSString *source;
@property (strong, nonatomic) NSString *category;
@property (strong, nonatomic) NSString *mobileUrl;


- (instancetype)initWithDealDictionary: (NSDictionary *)dealDictionary;

@end
