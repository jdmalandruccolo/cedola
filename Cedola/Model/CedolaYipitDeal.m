//
//  CedolaYipitDeal.m
//  Cedola
//
//  Created by Joseph Malandruccolo on 4/29/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import "CedolaYipitDeal.h"
#import "CedolaAPIManager.h"


//  yipit api json keys
NSString * const    kYipitDealIdKey = @"id";
NSString * const    kYipitEndDateKey = @"end_date";
NSString * const    kYipitPriceKey = @"price";
NSString * const    kYipitFormattedPriceKey = @"formatted";
NSString * const    kYipitValueKey = @"value";
NSString * const    kYipitFormattedValueKey = @"formatted";
NSString * const    kYipitTitleKey = @"title";
NSString * const    kYipitYipitTitleKey = @"yipit_title";
NSString * const    kYipitImageKey = @"images";
NSString * const    kYipitSmallImageKey = @"image_small";
NSString * const    kYipitBigImageKey = @"image_big";
NSString * const    kYipitDescriptionKey = @"description";
NSString * const    kYipitBusinessKey = @"business";
NSString * const    kYipitLocationsKey = @"locations";
NSString * const    kYipitSourceKey = @"source";
NSString * const    kYipitSourceNameKey = @"name";
NSString * const    kYipitTagKey = @"tags";
NSString * const    kYipitSlugKey = @"slug";
NSString * const    kYipitMobileUrlKey = @"mobile_url";

@implementation CedolaYipitDeal


- (instancetype) initWithDealDictionary:(NSDictionary *)dealDictionary
{
    self = [super init];
    
    if (self) {
        //  parse dictionary
        self.dealId = [dealDictionary objectForKey:kYipitDealIdKey];
        self.endDate = [dealDictionary objectForKey:kYipitEndDateKey];
        
        NSDictionary *priceDict = [dealDictionary objectForKey:kYipitPriceKey];
        self.price = [priceDict objectForKey:kYipitFormattedPriceKey];
        
        NSDictionary *valueDict = [dealDictionary objectForKey:kYipitValueKey];
        self.value = [valueDict objectForKey:kYipitFormattedValueKey];
        
        self.title = [dealDictionary objectForKey:kYipitTitleKey];
        self.yipitTitle = [dealDictionary objectForKey:kYipitYipitTitleKey];
        
        NSDictionary *imageDict = [dealDictionary objectForKey:kYipitImageKey];
        self.smallImageUrl = [imageDict objectForKey:kYipitSmallImageKey];
        self.bigImageUrl = [imageDict objectForKey:kYipitBigImageKey];
        
        self.description = [dealDictionary objectForKey:kYipitDescriptionKey];
        
        NSDictionary *businessDict = [dealDictionary objectForKey:kYipitBusinessKey];
        NSArray *locations = [businessDict objectForKey:kYipitLocationsKey];
        self.address = [locations objectAtIndex:0];
        
        NSDictionary *sourceDict = [dealDictionary objectForKey:kYipitSourceKey];
        self.source = [sourceDict objectForKey:kYipitSourceNameKey];
        
        NSArray *tags = [dealDictionary objectForKey:kYipitTagKey];
        NSDictionary *tagDict = [tags objectAtIndex:0];
        self.category = [[CedolaAPIManager sharedAPIManager] categoryFromTag:[tagDict objectForKey:kYipitSlugKey]];
        
        self.mobileUrl = [dealDictionary objectForKey:kYipitMobileUrlKey];
        
    }
    
    return self;
}

@end
