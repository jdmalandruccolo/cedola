//
//  CedolaAPIManager.h
//  Cedola
//
//  Created by Joseph Malandruccolo on 4/29/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CedolaAPIManager : NSObject <CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
}

+ (instancetype)sharedAPIManager;


- (NSURLRequest *)yipitAPIFFromCurrentLocation;
- (NSURLRequest *)yipitAPIFromSource: (NSString *)source;   //  see Sources API
- (NSURLRequest *)yipitAPIFromTag: (NSString *)tag;         //  see Tags API
- (NSString *)categoryFromTag: (NSString *)tag;             //  see https://docs.google.com/document/d/1bMHyJoJYpBLIfhSiGVDzFceeOjPo5zMy5IOx5Jp8D-w/edit
- (NSArray *)validSearchTerms;


@end
