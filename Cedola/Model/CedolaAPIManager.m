//
//  CedolaAPIManager.m
//  Cedola
//
//  Created by Joseph Malandruccolo on 4/29/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import "CedolaAPIManager.h"

//  Yipit API constants
NSString * const    kYipitAPIsuffix = @"&key=pe6YPLZzJPEQwX2N";
NSString * const    kYipitAPIprefix = @"http://api.yipit.com/v1/deals/?";
NSString * const    kYipitAPIlatitudeParamater = @"lat=";
NSString * const    kYipitAPIlongitudeParameter = @"lon=";
NSString * const    kYipitAPIradiusParameter = @"radius=";
NSString * const    kYipitAPIlimitParameter = @"limit=";
NSString * const    kYipitAPIlimitInput = @"100";
NSString * const    kYipitAPItagParameter = @"tag=";


//  HTTP protocol constants
NSString * const    kAPIparameterDelimiter = @"&";

@interface CedolaAPIManager ()

@property (strong, nonatomic) NSDictionary *tagCategoryMap;

@end


@implementation CedolaAPIManager


#pragma mark - singleton methods
+ (instancetype)sharedAPIManager
{
    static CedolaAPIManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[CedolaAPIManager alloc] init];
        
         sharedManager.tagCategoryMap = [[NSDictionary alloc] initWithObjectsAndKeys:@"activities-adventures", @"bowling",
                                         @"activities-adventures", @"city-tours",
                                         @"activities-adventures", @"comedy-clubs",
                                         @"activities-adventures", @"dance-classes",
                                         @"activities-adventures", @"golf",
                                         @"activities-adventures", @"life-skills-classes",
                                         @"activities-adventures", @"musemus",
                                         @"activities-adventures", @"outdoor-adventures",
                                         @"activities-adventures", @"theater",
                                         @"activities-adventures", @"skiing",
                                         @"activities-adventures", @"skydiving",
                                         @"activities-adventures", @"sporting-events",
                                         @"activities-adventures", @"wine-tasting",
         
                                         @"dining-nightlife", @"bar-club",
                                         @"dining-nightlife", @"restaurants",
         
                                         @"fitness", @"boot-camp",
                                         @"fitness", @"fitness-classes",
                                         @"fitness", @"gym",
                                         @"fitness", @"martial-arts",
                                         @"fitness", @"personal-training",
                                         @"fitness", @"pilates",
                                         @"fitness", @"yoga",
                                         
                                         @"health-beauty", @"chiropractic",
                                         @"health-beauty", @"dental",
                                         @"health-beauty", @"dermatology",
                                         @"health-beauty", @"eye-vision",
                                         @"health-beauty", @"facial",
                                         @"health-beauty", @"hair-removal",
                                         @"health-beauty", @"hair-salon",
                                         @"health-beauty", @"makeup",
                                         @"health-beauty", @"manicure-pedicure",
                                         @"health-beauty", @"massage",
                                         @"health-beauty", @"spa",
                                         @"health-beauty", @"tanning",
                                         @"health-beauty", @"teeth-whitening",
                                         
                                         @"retail-services", @"automotive-services",
                                         @"retail-services", @"food-grocery",
                                         @"retail-services", @"home-services",
                                         @"retail-services", @"mens-clothing",
                                         @"retail-services", @"photography-services",
                                         @"retail-services", @"treats",
                                         @"retail-services", @"womens-clothing",
                                         
                                         @"baby", @"baby",
                                         @"college", @"college",
                                         @"pets", @"pets",
                                         @"gay", @"gay",
                                         @"kids", @"kids",
                                         @"bridal", @"bridal",
                                         @"travel", @"travel",
                                         @"jewish", @"jewish"
         
        
                                         , nil];


    });
    
    
    return sharedManager;
}


#define METERS_PER_MILE 1609.34f
- (instancetype)init
{
    self = [super init];
    if (self) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.distanceFilter = 5.0f * METERS_PER_MILE;
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        [locationManager startUpdatingLocation];
    }
    return self;
}


#pragma mark - public api
- (NSURLRequest *)yipitAPIFFromCurrentLocation
{
    //  get the user's current location
    CLLocation *currentLocation = [locationManager location];
    NSString *latitude = [NSString stringWithFormat:@"%f", currentLocation.coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", currentLocation.coordinate.longitude];
    NSLog(@"Current location: %@", currentLocation);
    
    //  build the api query string
    NSMutableString *apiQueryString = [[NSMutableString alloc] initWithString:kYipitAPIprefix];
    [apiQueryString appendString:kYipitAPIlatitudeParamater];
    [apiQueryString appendString:latitude];
    [apiQueryString appendString:kAPIparameterDelimiter];
    
    [apiQueryString appendString:kYipitAPIlongitudeParameter];
    [apiQueryString appendString:longitude];
    [apiQueryString appendString:kAPIparameterDelimiter];
    
    [apiQueryString appendString:kYipitAPIlimitParameter];
    [apiQueryString appendString:kYipitAPIlimitInput];
    
    [apiQueryString appendString:kYipitAPIsuffix];
    
    
    //  fetch the api query
    NSURL *apiUrl = [NSURL URLWithString:[NSString stringWithString:apiQueryString]];
    NSURLRequest *apiRequest = [NSURLRequest requestWithURL:apiUrl];
    
    return apiRequest;
}


- (NSURLRequest *)yipitAPIFromTag:(NSString *)tag
{
    //  get the user's current location
    CLLocation *currentLocation = [locationManager location];
    NSString *latitude = [NSString stringWithFormat:@"%f", currentLocation.coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", currentLocation.coordinate.longitude];
    NSLog(@"Current location: %@", currentLocation);
    
    //  build the api query string
    NSMutableString *apiQueryString = [[NSMutableString alloc] initWithString:kYipitAPIprefix];
    [apiQueryString appendString:kYipitAPIlatitudeParamater];
    [apiQueryString appendString:latitude];
    [apiQueryString appendString:kAPIparameterDelimiter];
    
    [apiQueryString appendString:kYipitAPIlongitudeParameter];
    [apiQueryString appendString:longitude];
    [apiQueryString appendString:kAPIparameterDelimiter];
    
    [apiQueryString appendString:kYipitAPIlimitParameter];
    [apiQueryString appendString:kYipitAPIlimitInput];
    [apiQueryString appendString:kAPIparameterDelimiter];
    
    [apiQueryString appendString:kYipitAPItagParameter];
    [apiQueryString appendString:tag];
    
    
    [apiQueryString appendString:kYipitAPIsuffix];
    
    //  fetch the api query
    NSURL *apiUrl = [NSURL URLWithString:[NSString stringWithString:apiQueryString]];
    NSURLRequest *apiRequest = [NSURLRequest requestWithURL:apiUrl];
    
    return apiRequest;

}



- (NSString *)categoryFromTag:(NSString *)tag
{
    return [self.tagCategoryMap objectForKey:tag];
}


- (NSArray *)validSearchTerms
{
    NSMutableArray *searchTerms = [[NSMutableArray alloc] initWithArray:[NSArray arrayWithArray:[self.tagCategoryMap allKeys]]];
    [searchTerms addObjectsFromArray:[NSArray arrayWithObjects:@"activities-adventures", @"dining-nightlife", @"fitness", @"health-beauty", @"retail-services", nil]];
    return [searchTerms sortedArrayUsingSelector:@selector(compare:)];
}


#pragma mark - CLLocationDelegate methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"Current location: %@", [locations lastObject]);
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Location services not enabled" message:@"Enable location services to let Cedola find deals near you!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}


@end
