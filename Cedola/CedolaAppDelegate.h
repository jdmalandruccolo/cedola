//
//  CedolaAppDelegate.h
//  Cedola
//
//  Created by Joseph Malandruccolo on 4/26/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CedolaAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
