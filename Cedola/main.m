//
//  main.m
//  Cedola
//
//  Created by Joseph Malandruccolo on 4/26/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CedolaAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CedolaAppDelegate class]));
    }
}
