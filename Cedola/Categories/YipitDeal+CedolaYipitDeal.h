//
//  YipitDeal+CedolaYipitDeal.h
//  Cedola
//
//  Created by Joseph Malandruccolo on 5/1/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import "YipitDeal.h"
#import "CedolaYipitDeal.h"

@interface YipitDeal (CedolaYipitDeal)

+ (YipitDeal *)yipitDealWithCedolaYipitDeal:(CedolaYipitDeal *)deal
                     inManagedObjectContext:(NSManagedObjectContext *)context;

@end
