//
//  YipitDeal+CedolaYipitDeal.m
//  Cedola
//
//  Created by Joseph Malandruccolo on 5/1/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import "YipitDeal+CedolaYipitDeal.h"

@implementation YipitDeal (CedolaYipitDeal)


+ (YipitDeal *)yipitDealWithCedolaYipitDeal:(CedolaYipitDeal *)deal
                     inManagedObjectContext:(NSManagedObjectContext *)context
{
    YipitDeal *dbDeal = nil;
    
    
    //  query the DB and confirm that an object with this deal id isn't already in the DB before writing (only want one instance in DB
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"YipitDeal"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"dealId" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"dealId = %@", deal.dealId];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || [matches count] > 1) {
        //  handle error
    }
    else if (![matches count]) {
        
        dbDeal = [NSEntityDescription insertNewObjectForEntityForName:@"YipitDeal" inManagedObjectContext:context];
        
        dbDeal.dealId = deal.dealId;
        dbDeal.endDate = deal.endDate;
        dbDeal.price = deal.price;
        dbDeal.value = deal.value;
        dbDeal.title = deal.title;
        dbDeal.yipitTitle = deal.yipitTitle;
        dbDeal.smallImageUrl = deal.smallImageUrl;
        dbDeal.bigImageUrl = deal.bigImageUrl;
        dbDeal.description_cedola = deal.description;
        dbDeal.source = deal.source;
        dbDeal.category = deal.category;
        dbDeal.mobileUrl = deal.mobileUrl;
        
    }
    else {
        dbDeal = [matches lastObject];
    }
    
    
    
    return dbDeal;
    
}

@end
