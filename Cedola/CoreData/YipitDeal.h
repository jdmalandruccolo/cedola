//
//  YipitDeal.h
//  Cedola
//
//  Created by Joseph Malandruccolo on 5/1/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface YipitDeal : NSManagedObject

@property (nonatomic, retain) NSString * dealId;
@property (nonatomic, retain) NSString * endDate;
@property (nonatomic, retain) NSString * price;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * yipitTitle;
@property (nonatomic, retain) NSString * smallImageUrl;
@property (nonatomic, retain) NSString * bigImageUrl;
@property (nonatomic, retain) NSString * description_cedola;
@property (nonatomic, retain) NSString * source;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * mobileUrl;

@end
