//
//  YipitDeal.m
//  Cedola
//
//  Created by Joseph Malandruccolo on 5/1/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import "YipitDeal.h"


@implementation YipitDeal

@dynamic dealId;
@dynamic endDate;
@dynamic price;
@dynamic value;
@dynamic title;
@dynamic yipitTitle;
@dynamic smallImageUrl;
@dynamic bigImageUrl;
@dynamic description_cedola;
@dynamic source;
@dynamic category;
@dynamic mobileUrl;

@end
